// import { StatusBar } from 'expo-status-bar';
// import { StyleSheet, Text, View } from 'react-native';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.js to start working on your app!</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

// import { StatusBar } from 'expo-status-bar';
// import React from 'react';
// import { Text, View , TouchableOpacity } from 'react-native';

// export default function App() {
//   return (
//    <View className="flex-1 items-center justify-center bg-zinc-950">
//       <TouchableOpacity className="bg-slate-500 w-32 items-center justify-center h-10 rounded-md"><Text className="text-xl text-white">click me</Text></TouchableOpacity>
//   </View>
//   );
// }

// In App.js in a new project


import {Provider} from "react-redux"
import MyApp from './grnApp/navigation/myApp';
import store from './grnApp/store/store';

function Root() {
  return (
    <Provider store={store}>
        <MyApp />
    </Provider>
  );
}

export default Root;
