
import { TouchableOpacity, View , Text } from 'react-native';
import MyApp from './grnApp/navigation/myApp';

function Test() {
  return (
//        <MyApp />
   <View className="flex-1 items-center justify-center bg-zinc-950">
      <TouchableOpacity className="bg-slate-500 w-32 items-center justify-center h-10 rounded-md"><Text className="text-xl text-white">click me</Text></TouchableOpacity>
  </View>
  );
}

export default Test;

