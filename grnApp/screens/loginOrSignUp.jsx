import React , { useState , useEffect}  from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { View, Text , TouchableOpacity , ScrollView , TextInput } from 'react-native';
import { useFonts } from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import { Formik }  from "formik";
import {MaterialIcons , Ionicons  } from '@expo/vector-icons';
import Layout from "../components/layout"
import styleSheet from "../styles/styleSheet";


function LoginOrSignUp() {
  return (
    <Layout 
    component={<LoginOrSignUpScreen />}
    />
    );
  }
  

function LoginOrSignUpScreen(){

  let [selectEmail , setSelectEmail] = useState(true);
  let [selectPhoneNumber , setSelectPhoneNumber] = useState(false);

  let [buttonsHighliting , setButtonsHighliting ] = useState({email : true , phoneNumber : false});


  const [fontsLoaded] = useFonts({
    'Heebo': require('../../assets/fonts/Heebo-VariableFont_wght.ttf'),
  });

  useEffect(()=>{
    async function prepareFonts(){
      await SplashScreen.preventAutoHideAsync();
    }
    prepareFonts();
  },[])

  if(!fontsLoaded){
    return undefined;
  }else{
    SplashScreen.hideAsync();
  }
// bg-slate-200
  return(
    <View>
      <Text className="mt-16 text-center font-bold text-white" style={styleSheet.login_signup_heading}>Sign up</Text>

      { /* email or phoneNumber */ }
      <View className="flex flex-row gap-2 mt-3 justify-center">

        <TouchableOpacity className={`w-16 h-9 items-center justify-center rounded-md border-2 ${buttonsHighliting.email ? "border-white bg-green-700" : ""}`} onPress={()=>{setButtonsHighliting({email : true , phoneNumber : false}) }} >
           <Text className={`text-s font-bold ${buttonsHighliting.email ? "text-white" : ""}`}>Email</Text>
        </TouchableOpacity>

        <TouchableOpacity className={`w-28 h-9 items-center justify-center rounded-md border-2 ${buttonsHighliting.phoneNumber ? "border-white bg-green-700" : ""} `} onPress={()=>{setButtonsHighliting({email : false , phoneNumber : true}) }} >
           <Text className={`text-s font-bold ${buttonsHighliting.phoneNumber ? "text-white" : ""}`}>Phone Number</Text>
        </TouchableOpacity>

      </View>


      {/* signUp form */}
      <View>
        <Formik 
          initialValues={buttonsHighliting.email ? {email : '' , password : ''} : {phoneNumber : '' , password : ''}}
          onSubmit={()=>{console.log(123)}}
          >
            {({ handleChange, handleBlur, handleSubmit, values }) => (

                <View className="items-center mt-8 space-y-6">

                { 
                  buttonsHighliting.email &&           
                   <View className='flex flex-row bg-white p-2 space-x-3 rounded-xl items-center border-2 border-gray-500' style={[styleSheet.textBoxViewWidth]}>
                         <MaterialIcons name="email" size={24} color="black" />
                        <TextInput placeholder='Email' className="text-xl" />
                    </View>
                }

                { 
                  buttonsHighliting.phoneNumber &&           
                   <View className='flex flex-row bg-white p-2 space-x-3 rounded-xl items-center border-2 border-gray-500' style={[styleSheet.textBoxViewWidth]}>
                         <MaterialIcons name="local-phone" size={24} color="black" />
                        <TextInput placeholder='Phone Number' className="text-xl" keyboardType='phone-pad' />
                    </View>
                }

                    <View className='flex flex-row bg-white p-2 space-x-3 rounded-xl items-center border-2 border-gray-500' style={[styleSheet.textBoxViewWidth]}>
                    <MaterialIcons name="lock" size={24} color="black" />
                      <TextInput placeholder='Password' className="text-xl" style={[styleSheet.textBoxWidth]} secureTextEntry={true} />
                      <Ionicons name="eye" size={24} color="black" className="ml-9" />
                    </View>
                </View>



            )}



        </Formik>
      </View>


    </View>
  )
}

export default LoginOrSignUp;