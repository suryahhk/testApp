import React , { useState , useEffect}  from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { View, Text , TouchableOpacity , ScrollView , TextInput } from 'react-native';
import { useFonts } from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import { useDispatch , useSelector } from "react-redux";
import { Formik }  from "formik";
import {MaterialIcons , Ionicons  } from '@expo/vector-icons';
import axios from "axios";
import Layout from "../components/layout"
import styleSheet from "../styles/styleSheet";
import { fitBitFetchHTMLData } from '../store/reducers/fitBitReducer';


function Testing(props) {
    
    return (
    <Layout
        component={<TextingScreen {...props} />}
        classNames={"items-center justify-center"}
    />
    );
  }

  function TextingScreen(props){

    
    let dispatch = useDispatch();
//    let Selector =  useSelector(state=>state);

    React.useEffect(()=>{ },[])

    function print() {

            props.navigation.navigate('FitBit');

        // axios.get("http://125.62.194.126:8080/fitbitdev/").then((res)=>{
        //     props.navigation.navigate('FitBit', {
        //         HtmlData: res.data,
        //       });
        // }).catch((err)=>{
        //     console.log(err)
        // })

//        dispatch(fitBitFetchHTMLData())
    }


    return(
        <View>
            <TouchableOpacity className="bg-white w-32 items-center justify-center h-10 rounded-md" onPress={()=>{print()}}><Text className="text-xl text-black font-extrabold">Sync</Text></TouchableOpacity>
        </View>
    )
  }


  export default Testing;