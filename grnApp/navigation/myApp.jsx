// import { StatusBar } from 'expo-status-bar';
// import { StyleSheet, Text, View } from 'react-native';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.js to start working on your app!</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

// import { StatusBar } from 'expo-status-bar';
// import React from 'react';
// import { Text, View , TouchableOpacity } from 'react-native';

// export default function App() {
//   return (
//    <View className="flex-1 items-center justify-center bg-zinc-950">
//       <TouchableOpacity className="bg-slate-500 w-32 items-center justify-center h-10 rounded-md"><Text className="text-xl text-white">click me</Text></TouchableOpacity>
//   </View>
//   );
// }

// In App.js in a new project



import React , {useEffect}  from 'react';
import { StatusBar } from 'expo-status-bar';
import * as Updates from 'expo-updates';
import { Alert , Text, View } from "react-native";
import NetInfo from '@react-native-community/netinfo';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// screens
import HomeScreen from '../screens/loginOrSignUp';
import LoginOrSignUp from  "../screens/loginOrSignUp";
import Testing from '../screens/Testing';
import FitBit from '../screens/FitBit';


const Stack = createNativeStackNavigator();

function MyApp() {

  const [netINfo , setNetInfo] = React.useState(null);
  const [test , setTest] = React.useState('')
 

    useEffect(() => {
      const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
        if(state.isConnected == false){
          Alert.alert("No Internet" , "please re-connect" , [{
            text:'Restart' , onPress : () => Updates.reloadAsync()
          }]);
        }else{
          setNetInfo(true)          
        }
      });
      return () => removeNetInfoSubscription();
  },[]);

  // netINfo == true ?
  // <Text>surya</Text>:
  // <Text>teja</Text>


  return (
  netINfo &&
    <NavigationContainer>
        <StatusBar backgroundColor='white' />
      <Stack.Navigator initialRouteName={"Testing"}>
        <Stack.Screen name="Home" options={{ headerShown : false }} component={LoginOrSignUp} />
        <Stack.Screen name="Testing" options={{ headerShown : false }} component={Testing} />
        <Stack.Screen name="FitBit" options={{ headerShown : false }} component={FitBit} />
        </Stack.Navigator>
    </NavigationContainer>

  );
}

export default MyApp;

// setTest(

//   `Connection type: ${state.type}
// Is connected?: ${state.isConnected}
// IP Address: ${state.details.ipAddress}`

// );