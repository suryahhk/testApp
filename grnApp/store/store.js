import { configureStore } from "@reduxjs/toolkit";

// reducers
import  userReducers  from "./reducers/userReducer";
import fitBitReducer from "./reducers/fitBitReducer";


let store = configureStore({
    reducer : {
        userReducers,
        fitBitReducer
    }
})

export default store;



