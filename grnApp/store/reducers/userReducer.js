import {  createSlice , createAsyncThunk } from "@reduxjs/toolkit";

let initialState = {
    data : null,
    isLoading : null,
    isError : null
};

let userSlice = createSlice({
    name : 'user',
    initialState,
    reducers:{},
    // extraReducers:{

    // }
})

export let userLogin = createAsyncThunk('user/login' , async ()=>{

})

let userReducers = userSlice.reducer;
export default userReducers;