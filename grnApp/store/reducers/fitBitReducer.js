import {  createSlice , createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

let initialState = {
    data : null,
    isLoading : false,
    isError : false
};

let fitBitSlice = createSlice({
    name : 'fitBit',
    initialState,
    reducers:{},
    extraReducers:(builder)=>{
        builder.addCase(fitBitFetchHTMLData.pending,(state,action)=>{
            state.isLoading = true;
        }),
        builder.addCase(fitBitFetchHTMLData.rejected ,(state,action)=>{
            state.isLoading = false;
            state.isError = true;
            state.data = null;
        }),
        builder.addCase(fitBitFetchHTMLData.fulfilled , (state , action)=>{
            state.isLoading = false ; state.isError=false;
            state.data=action.payload;
        })
    }
});

export let fitBitFetchHTMLData = createAsyncThunk(
    'fitBit/fetchData',
     async (...thunkAPI)=>{
       let data = await axios.get('http://125.62.194.126:8080/fitbitdev/').then((data)=>{
            return data.data;
        }).catch((err)=>{
            alert(1);
             return thunkAPI[1].rejectWithValue('error');
        })
        return data;
     }
)

let fitBitReducer = fitBitSlice.reducer;
export default fitBitReducer