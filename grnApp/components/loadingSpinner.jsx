import { ActivityIndicator, View  , Modal } from "react-native";

let LoadingSpinner = (props)=>{
    return(
        <Modal transparent visible={props.visible}>
                <View style={{flex:1 , alignSelf : 'center' , justifyContent : 'center'}} >
                        <ActivityIndicator size="large" />
                </View>
        </Modal>
    )
}

export default LoadingSpinner;
