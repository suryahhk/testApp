import { StyleSheet  } from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

let styleSheet = StyleSheet.create({
      login_signup_heading : {
        fontSize : hp('8%') ,
        fontFamily : 'Heebo',
        textShadowColor: 'black',
    textShadowRadius: 3,
    textShadowOffset: { 
      width: 1,
      height: 1,
    },
    },
    textBoxViewWidth : {
      width : wp(94)
    },
    textBoxWidth : {
      width : wp(70)
    },
    fitBitWigert : {
      width : wp("96%"),
      height : hp("94%"),
      padding:"0.5%",
      margin : "2%",
      borderBlockColor:'green',
      borderWidth:1
    }
})

export default styleSheet;