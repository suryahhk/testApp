// /** @type {import('tailwindcss').Config} */
// module.exports = {
//   content: [],
//   theme: {
//     extend: {},
//   },
//   plugins: [],
// }

// tailwind.config.js

module.exports = {
 content: [
            "./Root.{js,jsx,ts,tsx}" , 
            "./<custom directory>/**/*.{js,jsx,ts,tsx}" , 
            "./grnApp/screens/**/*.{js,jsx,ts,tsx}" , 
            "./grnApp/navigation/**/*.{js,jsx,ts,tsx}",
            "./grnApp/components/**/*.{js,jsx,ts,tsx}"
          ],
    theme: {
      // colors : {
      //   // Configure your color palette here
      //   'layout' : '#2EBDDC'
      // }
      // ,
      extend: {},
    },
    plugins: [],
}